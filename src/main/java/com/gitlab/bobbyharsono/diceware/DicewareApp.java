package com.gitlab.bobbyharsono.diceware;

import java.io.IOException;

public class DicewareApp {
	private static DiceProcessor diceProcessor;

	private static void printHeader() {
		StringBuilder sb = new StringBuilder();
		sb.append("-----------------------------------------------------------\n");
		sb.append("Diceware Application 0.1").append("\n");
		sb.append("Author - Bobby").append("\n");
		sb.append("\nUsage: DicewareApplication <isSalted> <numOfSide> <numOfPassphrase>"+
				   "\nisSalted: true/false, whether adding some salt in your passphrase or not"+
				   "\nnumOfDice: true/false, how many dice would you want to use, minimum is 5"+
		 		   "\nnumOfPhrases: true/false, how many passphrases would you want to generate, default is 6");
		sb.append("\n-----------------------------------------------------------\n");
		
		System.out.println(sb.toString());
	}

	private static void printFooter() {
		System.out.println("\n--- Thank you");
	}

	public static void main(String[] args) throws IOException {
		printHeader();
		
		try{
			// If there are parameter supplied, use them
			diceProcessor = new DiceProcessor();
			
			if(null!=args){
				System.out.println("\n\nFinal Passphrases --> "+
				diceProcessor.process(
						null!=args[0] || !args[0].isEmpty() && "TRUE".equalsIgnoreCase(args[0]) ? true : false
						, null!=args[1] || !args[1].isEmpty() ? Integer.parseInt(args[1]) : 0
						, null!=args[2] || !args[2].isEmpty() ? Integer.parseInt(args[2]) : 0
								)
				);
			}
		}
		finally{
			diceProcessor.destroy();
			printFooter();
			System.exit(0);
		}
	}
}
