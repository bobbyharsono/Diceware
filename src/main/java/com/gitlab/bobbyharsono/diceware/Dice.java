package com.gitlab.bobbyharsono.diceware;

import java.util.Random;

/**
 * @author Bobby
 * <p>
 * POJO representing a dice with default side is 6
 * </p>
 * *
 */ 
public class Dice {
	
	/** The id of the dice, it will be used for processing only */
	private String id;
	
	/** How many side for the dice. */
	private int side = 6;
	
	/** Record of last roll's number. */
	private int lastRoll;

	/**
	 * Instantiates a new dice.
	 */
	public Dice() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new dice.
	 *
	 * @param id the id
	 */
	public Dice(String id) {
		super();
		this.id = id;
	}

	/**
	 * Roll.
	 *
	 * @return the rolled number
	 */
	public int roll() {
		lastRoll = new Random().nextInt(side) + 1;
		System.out.println("Dice [" + (this.id) + "] rolled number ["
				+ (this.lastRoll) + "]");
		return lastRoll;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the side.
	 *
	 * @return the side
	 */
	public int getSide() {
		return side;
	}

	/**
	 * Sets the side.
	 *
	 * @param side the new side
	 */
	public void setSide(int side) {
		this.side = side;
	}

	/**
	 * Gets the last roll.
	 *
	 * @return the last roll
	 */
	public int getLastRoll() {
		return lastRoll;
	}

	/**
	 * Sets the last roll.
	 *
	 * @param lastRoll the new last roll
	 */
	public void setLastRoll(int lastRoll) {
		this.lastRoll = lastRoll;
	}

}
