# Diceware

A small Java application simulating passphrase generation using Diceware concept

![pipeline status](https://gitlab.com/bobbyharsono/Diceware/badges/master/pipeline.svg)

[![coverage report](https://gitlab.com/bobbyharsono/Diceware/badges/master/coverage.svg)](https://gitlab.com/bobbyharsono/Diceware/commits/master)


## Getting Started

Simply clone the repository and build it using Maven, you will get a jar file

You can call the service by:


```
java -jar Diceware.jar --isSalted --numOfDice --numOfPhrases

** isSalted (boolean) whether adding additional 'salt' to final result or not 

** numOfDice (integer) how many dice would you want to use, minimum is 5

** numOfPhrases (integer) how many passphrases would you want to generate, default is 6

i.e. java -jar Diceware.jar true 5 6
```

### Prerequisites

Implemented using Oracle JDK 7


### Installing

Clone the project and build using Maven command

```
mvn clean package
```


## Running the tests

You can run the main class for testing the application by supplying proper command line arguments

## Deployment

Currently i haven't made any build config for this project, so you need to build it with yourself

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Bobby R. Harsono** - *Initial work* - [Gitlab](https://gitlab.com/bobbyharsono/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* https://pixabay.com/en/dice-die-probability-fortune-luck-147157/
* https://diceware.readme.io/
* http://world.std.com/~reinhold/diceware.html

