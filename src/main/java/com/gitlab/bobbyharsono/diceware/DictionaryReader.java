package com.gitlab.bobbyharsono.diceware;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DictionaryReader {

	private final String FILE_LOCATION = "diceware-wordlist.cnf";
	private final String SALT_LOCATION = "diceware-salt.cnf";

	private BufferedReader dictReader;
	private BufferedReader saltReader;

	private Map<String, String> dicewords;

	private Map<String, String> saltwords;

	public DictionaryReader() {
		super();
		this.init();
	}

	public void init() {
		// Preparing class loader to determine default location of dictionary
		// file
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream dictionaryStream = classLoader.getResourceAsStream(FILE_LOCATION);
		InputStream saltDictionaryStream = classLoader.getResourceAsStream(SALT_LOCATION);
		dicewords = new HashMap<String, String>();
		saltwords = new HashMap<String, String>();

		dictReader = new BufferedReader(new InputStreamReader(
				dictionaryStream));
		saltReader = new BufferedReader(new InputStreamReader(
				saltDictionaryStream));
		load();
		loadSalt();
		System.out.println("--> Dictionary files are OK. . .");
	}

	public void destroy() {
		try {
			dictReader.close();
			saltReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reload() {
		load();
		loadSalt();
	}

	private void load() {
		// Preparing regex to fetch only line started with 5 digits number and
		// grouping the value:
		// matcher[1] : digits (ID)
		// matcher[2] : word value
		Pattern p = Pattern.compile("(^\\d{5})\\s(\\w+)");
		Matcher m = null;

		// Read line per line while checking whether the digits matches
		// If yes, get the word value
		String line = null;
		try {
			while ((line = dictReader.readLine()) != null) {
				m = p.matcher(line);
				if (m.find()) {
					dicewords.put(m.group(1), m.group(2));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadSalt() {
		// Preparing regex to fetch only line started with 2 digits number and
		// grouping the value:
		// matcher[1] : digits (ID)
		// matcher[2] : word value
		Pattern p = Pattern.compile("(^\\d{2})\\s(\\S+)");
		Matcher m = null;

		// Read line per line while checking whether the digits matches
		// If yes, get the word value
		String line = null;
		try {
			while ((line = saltReader.readLine()) != null) {
				m = p.matcher(line);
				if (m.find()) {
					saltwords.put(m.group(1), m.group(2));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String search(String number, boolean isSalt) throws IOException {
		String retval = "";
		if (!isSalt) {
			// Iterate the dicewords collections
			retval = dicewords.get(number);
		} else {
			retval = saltwords.get(number);
		}
		return retval;
	}
}
