/*
 * 
 */
package com.gitlab.bobbyharsono.diceware;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * The Class DiceProcessor.
 * @author Bobby
 */
public class DiceProcessor {
	
	/** The collection of passphrases. */
	private Map<String, String> passphrases;

	/** The dictionary reader for reading list from dictionary file. */
	private DictionaryReader dictionaryReader;

	/** The default value of minimum dice counter. */
	private final int MIN_DICE_COUNTER = 5;

	/** The default value of  minimum word counter. */
	private final int MIN_WORD_COUNTER = 6;


	/**
	 * Instantiates a new dice processor.
	 */
	public DiceProcessor() {
		dictionaryReader = new DictionaryReader();
		passphrases = new LinkedHashMap<String, String>();
	}

	/**
	 * Destroy.
	 */
	public void destroy() {
		dictionaryReader.destroy();
	}

	/**
	 * Process.
	 *
	 * @param isSalted whether we are going to use salt on final phrases
	 * @param diceCounter the number of dice counter
	 * @param wordCounter the number of word counter
	 * @return final phrases
	 */
	public String process(boolean isSalted, int diceCounter, int wordCounter) {
		Dice dice = null;
		StringBuffer sb = new StringBuffer();
		
		// If counter is negative, set with default value
		if(diceCounter<=0) diceCounter = MIN_DICE_COUNTER;
		
		// If counter is negative, set with default value
		if(wordCounter<=0) wordCounter = MIN_WORD_COUNTER;

		
		for (int i = 0; i < wordCounter; i++) {
			// Instantiate 5 dices and roll them
			for (int j = 0; j < diceCounter; j++) {
				dice = new Dice(i+"");
				dice.roll();
				sb.append(dice.getLastRoll());
			}
			
			// Translate the digits and put it into collection
			try {
				String digits = sb.toString();
				String value = dictionaryReader.search(digits,false);
				if (null!=value && !value.isEmpty()) {
					System.out.println("Found match for [" + digits + "] - "+ value);
					passphrases.put(digits, value);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// Flush the buffer
				sb.delete(0, sb.length());
			}
		}

		// If you want to salted the result
		if (isSalted) {
			System.out.println("\nAdding salt. . .");
			
			// Get salt string
			String salt = this.getRandomSalt();
			
			// Roll once to determine which word to be salted
			int chosenWordIndex = new Random().nextInt(passphrases.size()-1);
			
			// Get that word and randomly determine the position
			int counter = 0;
			boolean found = false;
			String key = "";
			StringBuilder chosenWord = new StringBuilder();
			Iterator<Entry<String, String>> it = passphrases.entrySet().iterator();
			while(it.hasNext()&&!found){
				Entry<String,String> entry = it.next();
				if(counter==chosenWordIndex){
					key = entry.getKey();
					chosenWord.append(entry.getValue());
					found=true;
				}
				else{
					counter++;
				}
			}
			
			int chosenPositionIndex = new Random().nextInt(chosenWord.length()-1);
			
			// Update the passphrase and update the map
			chosenWord.insert(chosenPositionIndex, salt);
			System.out.println("Updated chosen key/word = ["+key+":"+chosenWord+"]");
			passphrases.put(key, chosenWord.toString());
			
		}

		// Extract the result
		Iterator<Entry<String, String>> it = passphrases.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			sb.append(entry.getValue());
			sb.append(" ");
		}

		// Flush the collection
		passphrases.clear();

		return sb.toString();
	}

	/**
	 * Gets the random salt.
	 *
	 * @return the random salt
	 */
	private String getRandomSalt() {
		String value = "";
		Dice dice = new Dice();
		dice.setId("saltDice");
		String coordinate = dice.roll() + "" + dice.roll() + "";
		try {
			value = dictionaryReader.search(coordinate,true);
			System.out.println("Salt is "+value);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
}
